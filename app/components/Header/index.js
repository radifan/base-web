/**
*
* Header
*
*/

import React from 'react';
import { Input, Menu } from 'semantic-ui-react'
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import s from './style.css';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Menu color="black" className={s.header} size="massive" fixed="top" secondary >
        <Menu.Item name="home" fitted >
          <span className={s.logo}>LOGO</span>
        </Menu.Item>
        <Menu.Menu position="right" >
          <Menu.Item fitted>
            <Input icon="search" placeholder="Search..." />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

Header.propTypes = {

};

export default Header;
