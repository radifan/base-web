/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.HomePage.header',
    defaultMessage: 'Welcome',
  },
  subHeader: {
    id: 'app.components.HomePage.subheader',
    defaultMessage: 'Great to see you here!',
  },
  form: {
    id: 'app.components.HomePage.form',
    defaultMessage: 'Login',
  },
  email: {
    id: 'app.components.HomePage.form.email',
    defaultMessage: 'Email',
  },
  password: {
    id: 'app.components.HomePage.form.password',
    defaultMessage: 'Password',
  },
  register: {
    id: 'app.components.HomePage.form.register',
    defaultMessage: 'Don\'t have account? Sign up here',
  },
  signIn: {
    id: 'app.components.HomePage.form.signIn',
    defaultMessage: 'Sign In',
  },
});
