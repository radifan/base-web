/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Link } from 'react-router';
import { Header, Form, Button } from 'semantic-ui-react';
import Helmet from 'react-helmet';
import LocaleToggle from 'containers/LocaleToggle';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title="React Base Project"
          meta={[
            { name: 'description', content: 'React Base Project' },
          ]}
        />
        <Header as="h2">
          <FormattedMessage {...messages.header} />
          <Header.Subheader>
            <FormattedMessage {...messages.subHeader} />
          </Header.Subheader>
        </Header>
        <Form>
          <Form.Field>
            <label htmlFor="email"><FormattedMessage {...messages.email} /></label>
            <input placeholder="Email" />
          </Form.Field>
          <Form.Field>
            <label htmlFor="password"><FormattedMessage {...messages.password} /></label>
            <input placeholder="Password" />
          </Form.Field>
          <Form.Field>
            <Link to="/register"><FormattedMessage {...messages.register} /></Link>
          </Form.Field>
          <Button fluid type="submit"><FormattedMessage {...messages.signIn} /></Button>
        </Form>
        <LocaleToggle />
      </div>
    );
  }
}
