/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  en: {
    id: 'boilerplate.containers.LocaleToggle.en',
    defaultMessage: 'English',
  },
  id: {
    id: 'boilerplate.containers.LocaleToggle.id',
    defaultMessage: 'Bahasa Indonesia',
  },
});
