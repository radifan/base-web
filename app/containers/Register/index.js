/*
 *
 * Register
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import { Link } from 'react-router';
import { Form, Header, Button } from 'semantic-ui-react';
import { createStructuredSelector } from 'reselect';
import makeSelectRegister from './selectors';
import messages from './messages';

export class Register extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { formatMessage } = this.props.intl;

    return (
      <div>
        <Helmet
          title="Register"
          meta={[
            { name: 'description', content: 'Description of Register' },
          ]}
        />
        <Header as="h2">
          <FormattedMessage {...messages.header} />
        </Header>
        <Form>
          <Form.Field>
            <label htmlFor="firstName"><FormattedMessage {...messages.firstName} /></label>
            <input placeholder={formatMessage(messages.firstName)} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="lastName"><FormattedMessage {...messages.lastName} /></label>
            <input placeholder={formatMessage(messages.lastName)} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="phoneNumber"><FormattedMessage {...messages.phoneNumber} /></label>
            <input placeholder={formatMessage(messages.phoneNumber)} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="email"><FormattedMessage {...messages.email} /></label>
            <input placeholder={formatMessage(messages.email)} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="password"><FormattedMessage {...messages.password} /></label>
            <input placeholder={formatMessage(messages.password)} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="confirmPassword"><FormattedMessage {...messages.confirmPassword} /></label>
            <input placeholder={formatMessage(messages.confirmPassword)} />
          </Form.Field>
          <Form.Field>
            <Link to="/"><FormattedMessage {...messages.signIn} /></Link>
          </Form.Field>
          <Button fluid type="submit"><FormattedMessage {...messages.submit} /></Button>
        </Form>
      </div>
    );
  }
}

Register.propTypes = {
  intl: intlShape.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Register: makeSelectRegister(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Register));
