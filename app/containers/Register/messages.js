/*
 * Register Messages
 *
 * This contains all the text for the Register component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Register.header',
    defaultMessage: 'Let\'s create your account now!',
  },
  firstName: {
    id: 'app.containers.Register.form.firstName',
    defaultMessage: 'First Name',
  },
  lastName: {
    id: 'app.containers.Register.form.lastName',
    defaultMessage: 'Last Name',
  },
  phoneNumber: {
    id: 'app.containers.Register.form.phoneNumber',
    defaultMessage: 'Phone Number',
  },
  email: {
    id: 'app.containers.Register.form.email',
    defaultMessage: 'Email',
  },
  password: {
    id: 'app.containers.Register.form.password',
    defaultMessage: 'Password',
  },
  confirmPassword: {
    id: 'app.containers.Register.form.confirmPassword',
    defaultMessage: 'Confirm Password',
  },
  submit: {
    id: 'app.containers.Register.button.submit',
    defaultMessage: 'Create Account',
  },
  signIn: {
    id: 'app.containers.Register.signIn',
    defaultMessage: 'Already have account? Sign in here',
  },
});
