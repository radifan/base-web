FROM node:6.11.0-alpine

RUN cd / \
	&& mkdir /app 

ADD ./ /app

RUN touch /app	

RUN npm install cross-env -g \
	&& cd /app \
	&& npm run setup

EXPOSE 50001

ENTRYPOINT npm start --prefix=/app -- --port 50001